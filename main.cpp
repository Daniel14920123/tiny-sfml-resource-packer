#include <iostream>
#include <fstream>
#include <filesystem>
#include <utility>
#include <sstream>
#include <iomanip>
#include <map>

using namespace std;
namespace fs = std::filesystem;

static std::string fileBeginning = 
    R"(
#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <vector>

namespace assets{
    struct StaticImage : public sf::Image{
        StaticImage(std::vector<char> data){
            this->loadFromMemory(data.data(), data.size());
        }
    };

    struct StaticTexture : public sf::Texture{
        StaticTexture(std::vector<char> data){
            this->loadFromMemory(data.data(), data.size());
        }
    };

    struct StaticSoundBuffer : public sf::SoundBuffer{
        StaticSoundBuffer(std::vector<char> data){
            this->loadFromMemory(data.data(), data.size());
        }
    };

    )";


template<typename T> 
constexpr std::istream& read(std::istream& is, T& value) { 
    return is.read(reinterpret_cast<char*>(&value), sizeof(T)); 
} 

enum class FileType {
    image,
    audio,
    unknown
};

FileType checkType(std::string name){
    std::string extension = name.substr(name.find_last_of('.') + 1, name.size() - name.find_last_of('.') - 1);
    if(extension == "png" 
    || extension == "tga" 
    || extension == "bmp" 
    || extension == "jpg" 
    || extension == "gif" 
    || extension == "psd" 
    || extension == "hdr"
    || extension == "pic")  return FileType::image;
    if(extension == "wav"
    || extension == "flac"
    || extension == "ogg")  return FileType::audio;
    return FileType::unknown;
}

std::vector<char> ReadAllBytes(std::string filename) {
    ifstream ifs(filename, ios::binary|ios::ate);
    ifstream::pos_type pos = ifs.tellg();
    std::vector<char> result(pos);
    ifs.seekg(0, ios::beg);
    ifs.read(&result[0], pos);

    return result;
}

std::string getName(std::string filename){
    return filename.substr(filename.find_last_of('/')+1, filename.find_last_of('.') - filename.find_last_of('/')-1);
}

std::string pack(std::string filename, FileType tp){
    std::vector<char> data = ReadAllBytes(filename);
    std::stringstream toReturn;
    if(tp == FileType::image){
        toReturn << "static StaticImage " << getName(filename) << "_img({ ";
        for(size_t i = 0; i < data.size() - 1; i++ ){
            toReturn << /*"0x" << std::hex << std::setfill('0') <<*/ to_string(static_cast<int8_t>(data[i])) << std::dec << ",";
        }
        toReturn << /*"0x" << std::hex << std::setfill('0') <<*/ to_string(static_cast<int8_t>(data[data.size() - 1])) << "});\n";

        toReturn << "static StaticTexture " << getName(filename) << "_tex({ ";
        for(size_t i = 0; i < data.size() - 1; i++ ){
            toReturn << /*"0x" << std::hex << std::setfill('0') <<*/ to_string(static_cast<int8_t>(data[i])) <<  std::dec << "," ;
        }
        toReturn << /*"0x" << std::hex << std::setfill('0') <<*/ to_string(static_cast<int8_t>(data[data.size() - 1])) << "});\n";
    } else if(tp == FileType::audio){
        toReturn << "static StaticSoundBuffer " << getName(filename) << "({ ";
        for(size_t i = 0; i < data.size() - 1; i++ ){
            toReturn << /*"0x" << std::hex << std::setfill('0') <<*/ to_string(static_cast<int8_t>(data[i])) << std::dec << ",";
        }
        toReturn << /*"0x" << std::hex << std::setfill('0') <<*/ to_string(static_cast<int8_t>(data[data.size() - 1])) << "});\n";

    }
    return toReturn.str();
}

int main(int argc, char** argv){
    fs::path path = fs::current_path();
    if(argc > 1) {
        std::string temp = argv[1];
        path/= temp;
    }

    std::string toWrite = fileBeginning;

    std::vector<std::string> fileList;
    for(auto& scrut : fs::recursive_directory_iterator(path)){
        if(scrut.is_regular_file()){
            std::string temp = scrut.path().string();
            FileType tp = checkType(temp);
            if(tp == FileType::image){
                std::cout << "PROCESSING IMAGE:\t" << scrut.path() << std::endl;
                toWrite = toWrite + pack(temp, tp);
            } else if(tp == FileType::audio){
                std::cout << "PROCESSING AUDIO:\t" << scrut.path() << std::endl;
                toWrite = toWrite + pack(temp, tp);
            } else{
                std::cout << "Unknown File type:\t" << scrut.path() << std::endl;
            }
        }
    }
    toWrite += "}";
    
    std::ofstream out("assets.hpp");
    out << toWrite << endl;
}