all:
	g++-8 main.cpp -o main.elf -std=c++17 -lstdc++fs -Wall

test:
	g++-8 test.cpp -o test.elf -std=c++17 -lsfml-graphics -lsfml-window -lsfml-system -lsfml-audio